const toNumber = require('./toNumber');

describe('toNumber', () => {
  test('1 => 1', () => {
    expect(toNumber(1)).toBe(1);
  });

  test('"-1" => -1', () => {
    expect(toNumber('-1')).toBe(-1);
  });

  test('"a" is error', () => {
    expect(() => {
      toNumber('a');
    }).toThrow('value \'a\' is not a number!');
  });

  test('"ff" is error', () => {
    expect(() => {
      toNumber('ff');
    }).toThrow('value \'ff\' is not a number!');
  });

  test('"200" => 200', () => {
    expect(toNumber('200')).not.toBe(3);
  });
});
