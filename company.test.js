const company = require('./company');

it('company.js test',()=>{
  expect(company(3,85)).toMatchSnapshot();
});

test('"a" is error', () => {
  expect(() => {
    company('a');
  }).toThrow('id needs to be integer');
});