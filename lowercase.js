module.exports = function (word) {
  const w = +word.toString.toLowerCase();
  if (typeof w !=='string') {
    throw new Error('bad input');
  }
  return w;
};